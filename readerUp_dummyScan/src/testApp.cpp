#include "testApp.h"

#define nbRandom 500

//--------------------------------------------------------------
void testApp::setup(){
    //
    ofSetLogLevel(OF_LOG_VERBOSE);
    
    //
    //gui.addTitle("Manipulating whole library");
    panel.setup();
    panel.add(btnEmpty.setup("Empty library"));
    panel.add(btnFill.setup("Fill Random"));
    panel.add(sizeOfFilling.setup("Size", 0, 0, 1000));
    
    //
    //gui.addTitle("Adding books");
    panel.add(btnAskAndAdd.setup("Ask and add"));
    panel.add(btnAddHyperionT1.setup("Add Hyperion T1"));
    panel.add(btnAddSlumDog.setup("Add SlumDog"));
    panel.add(btnAddJNobleBook.setup("Add J. Noble Book"));
    
    panel.add(btnSaveLibrary.setup("SAVE !"));
    
    //
    m_oLibrary.setup();
    m_oLibrary.loadFromXml(m_oLibrary.get_fileName());
    // We have to define the object for callback
    ofRegisterURLNotification(&m_oLibrary);
    
}


//--------------------------------------------------------------
void testApp::update(){
    
    if(btnEmpty==true)          m_oLibrary.clear(true);
    
    if(btnFill==true)           fillRandom();
    if(btnSaveLibrary)          m_oLibrary.saveToXml();
        
    if(btnAskAndAdd==true)      addABookWithQuestion();
    
    if(btnAddHyperionT1==true)       m_oLibrary.send(ofToString(ISBN13_HyperionTome1));
    if(btnAddSlumDog==true)          m_oLibrary.send(ofToString(ISBN13_SlumDog));
    if(btnAddJNobleBook==true)       m_oLibrary.send(ofToString(ISBN13_ProgrammingInteractivity));
    
}

//--------------------------------------------------------------
void testApp::draw(){
    
    // Display Library content
    string wholeLibrary = m_oLibrary.toString();
    ofDrawBitmapString(wholeLibrary, 10, ofGetHeight()*0.75);
    
    // Display Buttons
    if(bHide)   panel.draw();
}


//--------------------------------------------------------------
void testApp::addABookWithQuestion(){
    string isbnTyped;

    isbnTyped = ofSystemTextBoxDialog("Type the number on the back of your book");
    m_oLibrary.send(isbnTyped);
    
}

//--------------------------------------------------------------
void testApp::fillRandom(){


    double isbn = 0;
    double idxRandom;

    int titleNbChar = ofRandom(8, 60);
    int authorNbChar = ofRandom(15, 30);
    
    string title = "";
    string author = "";
    
    ruBook bookRandom;
    
    for(idxRandom=0; idxRandom<=sizeOfFilling; idxRandom++){

        bookRandom = ruBook();
        
        // Random isbn
        isbn = ofRandom(00000000000001, 9999999999999);

        // Random author
        author = "";
        for(int authorIdxChar=0; authorIdxChar<=authorNbChar; authorIdxChar++){
            author += (char)ofRandom(97, 122);
        }
        // Random title
        title = "";
        for(int titleIdxChar=0; titleIdxChar<=titleNbChar; titleIdxChar++){
            title += (char)ofRandom(97, 122);
        }
        
        bookRandom.set_isbnKey(ofToString(isbn,0,13,0));
        bookRandom.set_author(author);
        bookRandom.set_title(title);
        bookRandom.set_dateOfLastAdd(ofGetTimestampString("%Y%m%d%H%M%S%i"));
        
        // add it
        m_oLibrary.addABook(bookRandom);
        ofLogVerbose() << "Random Add [" << idxRandom << "]";
        
    }
    
    
}

//--------------------------------------------------------------
void testApp::keyPressed(int key){
    
    
}

//--------------------------------------------------------------
void testApp::keyReleased(int key){
    if(key=='$'){
        bHide = !bHide;
    }
}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y){
    
}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){
    
}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){
    
}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){
    
}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){
    
}

//--------------------------------------------------------------
void testApp::gotMessage(ofMessage msg){
    
}

//--------------------------------------------------------------
void testApp::dragEvent(ofDragInfo dragInfo){
    
}
