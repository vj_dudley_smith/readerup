#pragma once

#include "ofMain.h"

#include "ofxXmlSettings.h"
//#include "ofxSimpleGuiToo.h"
#include "ofxGui.h"

#include "ruLibrary.h"


// -------------------------------------------------------
// ISBN Numbers ------------------------------------------
// -------------------------------------------------------

// Programming interactivity - Joshua Noble
#define ISBN13_ProgrammingInteractivity 9780596154141
#define ISBN13_RaisonsEtSentiments      9782264058256
#define ISBN13_HyperionTome1            9782266111560
#define ISBN13_SlumDog                  9782264045531


class testApp : public ofBaseApp{
public:
    void setup();
    void update();
    void draw();
    
    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y);
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void windowResized(int w, int h);
    void dragEvent(ofDragInfo dragInfo);
    void gotMessage(ofMessage msg);
    
    // New stuff isbdb.com ----------------------------------
    ruLibrary       m_oLibrary;
    //
    ofxPanel    panel;
    
    ofxIntSlider   sizeOfFilling;
    
    ofxButton   btnEmpty;
    ofxButton   btnFill;

    ofxButton   btnAskAndAdd;
    ofxButton   btnAddHyperionT1;
    ofxButton   btnAddSlumDog;
    ofxButton   btnAddJNobleBook;
    
    ofxButton   btnSaveLibrary;

    bool        bHide;
    
    void addABookWithQuestion();
    void fillRandom();
    
};
