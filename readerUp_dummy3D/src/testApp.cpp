#include "testApp.h"

//--------------------------------------------------------------
void testApp::setup(){
    
    ofSetVerticalSync(true);
    
    m_oLibrary.setup();
    // Load the mesh
    m_oLibrary.loadMesh("lofi-bunny.ply");
    m_oLibrary.loadFromXml(m_oLibrary.get_fileName());
}

//--------------------------------------------------------------
void testApp::update(){

}

//--------------------------------------------------------------
void testApp::draw(){
 	
	
	cam.begin();

    m_oLibrary.drawAllBooks(150, 1.0f);
    
	cam.end();
}

//--------------------------------------------------------------
void testApp::keyPressed(int key){
    
    ofLog() << "Key pressed : " << key;

}

//--------------------------------------------------------------
void testApp::keyReleased(int key){
    
    switch (key) {
        case 's':
        case 'S':
            //m_oLibrary.saveToXml(m_oLibrary.get_fileName());
            break;
            
        case 'l':
        case 'L':
            //m_oLibrary.loadFromXml(m_oLibrary.get_fileName());
            break;
            
        default:
            break;
    }

}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y){

}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void testApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void testApp::dragEvent(ofDragInfo dragInfo){ 

}