//
//  ruBookDefines.h
//  readerUp
//
//  Created by Dudley Smith on 7/26/13.
//
//
#pragma once

#include "ruDefines.h"

#define strValueNotFound   "notFound"
#define intValueNotFound   0
#define floatValueNotFound   0.0

#define font_zapfino    "Zapfino.ttf"
#define font_agency     "AGENCY.TTF"
#define font_american_typewriter    "AmericanTypewriter.ttc"