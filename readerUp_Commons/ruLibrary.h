//
//  ruLibrary.h
//  readerUp_dummy3D
//
//  Created by Dudley Smith on 7/26/13.
//
//

#pragma once

#include "ofMain.h"

#include "ofxIsbndb.h"
#include "ofxColorSet.h"
#include "ofxMousePointers.h"

#include "ruLibraryDefines.h"

class ruLibrary : public ofxIsbndb{

public:
    ruLibrary();
    virtual ~ruLibrary();

    string toString();

private:
    map<string, ruBook>     m_aBooks;

    map<string, ruBook>     m_aBooks_dates;

    map<int,    string>     m_aBooksByVertices;

    map<string, ofPoint>        m_aOrderVerticesPosition;
    map<string, int>            m_aOrderVerticesIndex;

    vector<ofMesh>              m_aMeshes;
    int                         m_iIdxMesh;
    vector<ofMesh>::iterator    m_oCurrentMesh;
    
    string                  m_sFileName;


    ofxXmlSettings          m_oXml;
    
    
public:
    // Each set of colors needs 5 COLORS (From Dark to Bright) for those following uses
    // 0 : Pointers + BackGround for the message
    // 1 : Label
    // 2 : Points of the mesh (Back)
    // 3 : WireFrame (Back)
    // 4 : Points of the mesh (Front)
    // 5 : WireFrame (Front)
    // 6 : Message
    ofxColorSet             m_oColorSet;

    ofParameter<int>        *guiPx_AlphaPts;
    ofParameter<int>        *guiPx_AlphaWrf;

public:
    // Accessors ------------------------------------------
    map<string, ruBook> get_books(){return m_aBooks;};

    // no setter
    string get_fileName();

    // Methods relative to books --------------------------
    void addABook(string _title, string _author, string _isbn);
    void addABook(ruBook _roughBook);
    void addABook(string _key, ruBook _bookToAdd);

    // Mock of OF basic behavior ----------------------------
    void setup();
    void update();
    void drawAllBooks(float _size, float _scale);

    void drawOneMeshWireframe_Back(int _alpha);
    void drawOneMeshPoints_Back(int _alpha, float _pointSize);

    void drawOneMeshWireframe_Front(int _alpha, int _idxMesh = -1);
    void drawOneMeshPoints_Front(int _alpha, float _pointSize, int _idxMesh = -1);

    void drawAllMeshWireframe_Front(int _alpha, float _size, float _scale);
    void drawAllMeshPoints_Front(int _alpha, float _size, float _scale, float _pointSize);

    void getMeshMatrixPosition(int _idx, float _size, float _scale);

    void drawLabel(ofEasyCam &_cam, ofPoint _pointerPos);
    void drawIsbnMessage();

    // Loading/Saving as database
    void loadFromXml(string _path);
    void saveToXml();
    void saveToXml(string _path);

    // Clear, empty all books ------
    void clear(bool _saveToXml = false);

    // Overrides from ofxIsbn
    // Receive and add a book
    void    urlResponse(ofHttpResponse &_response);
    // Sending too
    //void    send(string _isbnNumber);

    // To load a mesh
    void loadMesh(string _fileName);
    void nextMesh();

    // ---------------------------------------------------
    // GAME PAD
private:
    ofTrueTypeFont          m_oFontMessage;
    ofTrueTypeFont          m_oFontMesh;
    ofTrueTypeFont          m_oFontLabel;
    
public:
    void loadFontMessage(string path);
    void loadFontMesh(string path);
    void loadFontLabel(string path);
    
    // ---------------------------------------------------
    
    // ---------------------------------------------------
    // GAME PAD
private:
    ofxMousePointer_Radar   m_oPointerBegin, m_oPointerEnd;

    // ---------------------------------------------------

    
    // ---------------------------------------------------
    // SUGGESTIONS DE BOUQUINS
private:
    vector<string>   m_aSuggestions;
    int              m_iLastSuggestionIdx;

    void    addSuggestion(string _suggestion);
    string  getASuggestionSentence();
    
    ruBook  getARandomBook();
    
    // ---------------------------------------------------


};
