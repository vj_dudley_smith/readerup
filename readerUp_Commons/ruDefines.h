//
//  ruDefines.h
//  readerUp_dummy3D
//
//  Created by Dudley Smith on 7/26/13.
//
//

#pragma once

// STD includes
#include <iostream>
// OF includes
#include "ofMain.h"
#include "ofxXmlSettings.h"

#ifdef TARGET_LINUX
    #define commonDataPath  "../../readerUp_Commons/datas/"
#else
    #define commonDataPath  "../../../../../readerUp_Commons/datas/"
#endif
