//
//  ruBook.cpp
//  readerUp_dummy3D
//
//  Created by Dudley Smith on 7/26/13.
//
//

#include "ruBook.h"

ruBook::ruBook(){
    init();
}
// Destructor -
ruBook::~ruBook(){
}


ruBook::ruBook(string _title, string _author, string _isbn){
    m_sTitle = _title;
    m_sAuthor = _author;
    
    // For the moment, no specific stuff, just position
    /*
    m_positionStart = giveStartPosition();
    m_positionEnd = giveStartPosition();
    */
    
    // For the moment, no specific color, we have to found a design
    //m_oColor = giveStartColor();
    
    // Isbn
    m_sIsbnKey = _isbn;
    
    init();
}

void ruBook::init(){


    
    // Setup animation ---------------------------
    m_oAnimScale.reset();
    m_oAnimScale.setCurve(LINEAR);
    m_oAnimScale.setRepeatType(PLAY_ONCE);
    m_oAnimScale.setDuration(1);
    m_oAnimScale.animateFromTo(0, 1);
    
    lastDraw = ofGetElapsedTimef();
    
}

string ruBook::toString(){
    
    string bookToString = "";
    
    bookToString = ofxIsbndbBook::toString() + " : ";
    
    bookToString += " Date =" + ofToString(m_sDateOfLastAdd);
    
    /*
    bookToString += "Pos End: ";
    
    bookToString += " X=" + ofToString(m_positionEnd.x) + " ";
    bookToString += " Y=" + ofToString(m_positionEnd.y) + " ";
    bookToString += " Z=" + ofToString(m_positionEnd.z) + " ";
    */
    
    return bookToString;
    
}

// Mock of OF basic behavior ----------------------------
void ruBook::drawMesh(ofTrueTypeFont &_font, ofPoint _position, ofColor _color){
    
    float dt = ofGetElapsedTimef() - lastDraw;
    lastDraw = ofGetElapsedTimef();
    
    m_oAnimScale.update(dt);
    
    if(_font.isLoaded()){
        
        ofPushMatrix();
        ofTranslate(_position);
        
        // Draw the text --------------------------------------------------
        ofPushStyle();
            ofSetColor(_color);
        
            float realScale = ofMap(m_oAnimScale.val(), 0, 1, 15, 1);
            ofScale(realScale, realScale, realScale);
        
            //_font.drawStringAsShapes(stringMesh_MiddleWord(), 0, 0);
            _font.drawStringAsShapes(m_sTitle, 0, 0);
        ofPopStyle();
        
        // Draw some animated circles --------------------------------------------------
        ofPushStyle();
            ofSetColor(_color);
            ofNoFill();
            ofSetLineWidth(10);
        
            float radius = ofMap(m_oAnimScale.val(), 0, 1, 500, 0);
        
            if(m_oAnimScale.isAnimating()>0){
                for(int idxCircle=0; idxCircle<8; idxCircle++){
                    ofSetCircleResolution(idxCircle*8);
                    ofCircle(0, 0, radius/(idxCircle+1));
                }
            }
        ofPopStyle();
        
        ofPopMatrix();
        
    }

}

// Mock of OF basic behavior ----------------------------
void ruBook::drawLabel(ofTrueTypeFont &_font, ofPoint _position, ofColor _color){
    
    if(_font.isLoaded()){
        
        ofPushMatrix();
        ofPushStyle();
        
        ofSetColor(_color);
        
        ofTranslate(_position);
        
        _font.drawStringAsShapes(stringLabel(), 0, 0);
        
        ofPopStyle();
        ofPopMatrix();
        
    }

    
}

string ruBook::stringMesh_Sign(){
    string str = "";
    
    str += m_sTitle[0];
    str += ":";
    str += m_sAuthor[0];
    
    return str;
}

string ruBook::stringMesh_Short(){
    string str = "";
    
    str += m_sTitle;
    str += " de ";
    str += m_sAuthor;
    
    return str;
}

string ruBook::stringMesh_MiddleWord(){
    string str = "";
    
    vector<string> aWords = ofSplitString(m_sTitle, " ");
    int middleIdx = 0.5*aWords.size();
    
    str = aWords[middleIdx];
    
    return str;
}

string ruBook::stringLabel(){
    string str = "";
    
    str += m_sTitle;
    str += "\n";
    str += m_sAuthor;
    str += "\n";
    str += m_sDateOfLastAdd;
    return str;

}
// XML Save ---------------------------------------------
void ruBook::saveToXml(ofxXmlSettings _xml, int _idxBook){
    
    if(!_xml.pushTag("book", _idxBook)){
        // Le Tag n'existe pas, on le crée
        _idxBook = _xml.addTag("book");
        _xml.pushTag("book", _idxBook);
    }
    
    _xml.setValue("isbn", m_sIsbnKey, _idxBook);
    
    _xml.setValue("title", m_sTitle, _idxBook);
    _xml.setValue("author", m_sAuthor, _idxBook);
    _xml.setValue("dateOfLastAdd", m_sDateOfLastAdd, _idxBook);
    _xml.setValue("idxMesh", m_iIdxMesh, _idxBook);

}

// XML Load ---------------------------------------------
bool ruBook::loadFromXml(ofxXmlSettings _xml, int _idxBook){
    
    if(!_xml.pushTag("book", _idxBook)){
        // Le Tag n'existe pas
        cerr << "Tag does not exist" << endl;
        return false;
    }
    
    m_sIsbnKey = _xml.getValue("isbn", strValueNotFound);
    
    m_sTitle = _xml.getValue("title", strValueNotFound);
    m_sAuthor = _xml.getValue("author", strValueNotFound);
    m_sDateOfLastAdd = _xml.getValue("dateOfLastAdd", strValueNotFound);
    m_iIdxMesh  = _xml.getValue("idxMesh", (int)ofRandom(0, 4));

    return true;
    
}