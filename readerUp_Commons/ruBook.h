//
//  ruBook.h
//  readerUp_dummy3D
//
//  Created by Dudley Smith on 7/26/13.
//
//

#pragma once

#include "ruBookDefines.h"

#include "ofxIsbndbBook.h"
#include "ofxAnimatableFloat.h"

class ruBook : public ofxIsbndbBook{
    
public:
    // Constructor(s) -
    ruBook();
    ruBook(string _title, string author, string _isbn);
    // Destructor -
    virtual ~ruBook();
    
    string toString();

private:
    // MEMBERS -------------------------------------------

    int                 m_iIdxMesh;
    
    ofxAnimatableFloat  m_oAnimScale;
    
    // Date of Last Add
    string      m_sDateOfLastAdd;
    
    float       lastDraw;
    
    
private:
    // Common loadings between the all accessors
    void init();
    
public:
    // ACCESSORS -------------------------------------------
    /*
    ofPoint get_positionStart(){return m_positionStart;}
    void set_positionStart(ofPoint _position){m_positionStart=_position;}
    
    ofPoint get_positionEnd(){return m_positionEnd;}
    void set_positionEnd(ofPoint _position){m_positionEnd=_position;}
    */

    string get_dateOfLastAdd(){return m_sDateOfLastAdd;}
    void set_dateOfLastAdd(string _dateOfLastAdd){m_sDateOfLastAdd=_dateOfLastAdd;}
    
    int get_idxMesh(){return m_iIdxMesh;}
    void set_idxMesh(int _idxMesh){m_iIdxMesh=_idxMesh;}
    
    /*
    ofColor get_color(){return m_oColor;}
    void set_color(ofColor _color){m_oColor=_color;}
    */
    
    string stringMesh_Sign();
    string stringMesh_Short();
    string stringMesh_MiddleWord();
    string stringLabel();
    
    // Mock of OF basic behavior ----------------------------
    void setup();
    void update();

public:
    void drawMesh(ofTrueTypeFont &_font, ofPoint _position, ofColor _color);
    void drawLabel(ofTrueTypeFont &_font, ofPoint _position, ofColor _color);
    void drawEmptyLabel(ofPoint _position, ofColor _color);
    
private:
    void draw(string _str, ofPoint _position, ofColor _color, ofTrueTypeFont &_font);

public:
    // XML Save/Load ---------------------------------------------
    void saveToXml(ofxXmlSettings _xml, int _idxBook);
    bool loadFromXml(ofxXmlSettings _xml, int _idxBook);
    
};
