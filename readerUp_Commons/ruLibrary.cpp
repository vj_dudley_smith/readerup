//
//  ruLibrary.cpp
//  readerUp_dummy3D
//
//  Created by Dudley Smith on 7/26/13.
//
//

#include "ruLibrary.h"

ruLibrary::ruLibrary(){

}
ruLibrary::~ruLibrary(){

}

string ruLibrary::get_fileName(){

    //ofFile pathControl = m_sFileName;

    return m_sFileName;

}

// Methods relative to books --------------------------
void ruLibrary::addABook(string _title, string _author, string _isbn){
    // Add with elementals infos
    addABook(ruBook(_title, _author, _isbn));
}

void ruLibrary::addABook(ruBook _roughBook){
    // Add with elemental inherited from rough book
    addABook( _roughBook.get_isbnKey(), _roughBook);
}

void ruLibrary::addABook(string _key, ruBook _bookToAdd){

    string keyDate = _bookToAdd.get_dateOfLastAdd();
    string keyAuth = _bookToAdd.get_author();
    string keyTitl = _bookToAdd.get_title();

    if(m_aBooks.find(_key) == m_aBooks.end()){
        // Add in array
        m_aBooks[_key] = _bookToAdd;
        // Log the addition operation
        ofLogVerbose() << "Book added in standard array : " << _bookToAdd.toString();
    }else{
        ofLogError() << "Book already in on this key : " << _key;
    }

    if(m_aBooks_dates.find(keyDate) == m_aBooks_dates.end()){
        // Add in array
        m_aBooks_dates[keyDate]     = _bookToAdd;
        // Log the addition operation
        ofLogVerbose() << "Book added in dates array : " << _bookToAdd.toString();
    }else{
        ofLogError() << "Book already in on this date : " << keyDate;
    }

    /*
    if(m_aBooks_authors.find(keyAuth) != m_aBooks.end()){
        // Add in array
        m_aBooks_authors[keyAuth]   = _bookToAdd;
    }

    if(m_aBooks_titles.find(keyTitl) != m_aBooks.end()){
        // Add in array
        m_aBooks_titles[keyTitl]    = _bookToAdd;
    }
    */



}

// Mock of OF basic behavior ----------------------------
void ruLibrary::setup(){

    ofFile pathControl;

    ofEnableDataPath();
    ofSetDataPathRoot(commonDataPath);

    // Check and notice
    pathControl = ofFile("library.xml");
    if(pathControl.exists() == true){
        m_sFileName = pathControl.getAbsolutePath();
        ofLogNotice() << "Library Path : " << pathControl.getAbsolutePath();
    }else{
        ofLogError() << "Library Path " << pathControl.getAbsolutePath() << " does not exist.";
    }

    // Load colors
    m_oColorSet.loadFromXml("colorSets.xml");

    // Load font
    loadFontMessage(font_american_typewriter);
    loadFontLabel(font_american_typewriter);
    loadFontMesh(font_american_typewriter);

    // Load Suggestions of books
    addSuggestion("Vous avez lu celui-ci ?");
    addSuggestion("A la page 32 de celui-ci, il y a des gros mots");
    addSuggestion("B. Pivot dit le plus grand bien de ");
    addSuggestion("Vous serez flatte par le style de ");
    addSuggestion("Connaissez vous ");
    addSuggestion("Il faut absolument lire  ");
    addSuggestion("Ne repartez pas sans ");
    addSuggestion("Demandez derriere vous ");
    addSuggestion("La bibliotheque doit aussi avoir ");
    addSuggestion("Je m’endors toujours grâce à ");
    addSuggestion("Mon frere a beaucoup aime ");
    addSuggestion("Ma soeur a beaucoup aime ");
    addSuggestion("Mon pere a beaucoup aime ");
    addSuggestion("Ma mere a beaucoup aime ");
    addSuggestion("Mon voisin a beaucoup aime ");
    addSuggestion("Ma voisine a beaucoup aime ");
    addSuggestion("Mon chien a beaucoup aime ");
    addSuggestion("Mon chat a beaucoup aime ");
    addSuggestion("Ma chatte a beaucoup aime ");
    addSuggestion("Mon boss a beaucoup aime ");
    addSuggestion("Mon patron a beaucoup aime ");
    addSuggestion("Tous les profs de francais conseillent ");
    addSuggestion("Quel beau roman que ");
    addSuggestion("Quelle belle histoire que ");
    addSuggestion("On n’est pas un homme si on a pas lu ");
    addSuggestion("Waouh, pfiou ");

    m_oPointerBegin.setup(false);
    m_oPointerEnd.setup(false);

}

void ruLibrary::update(){

}

// Load Font For little Label sticked to the mouse ------------------------------------
void ruLibrary::loadFontMessage(string _path){
    // Loading font
    ofFile fontFile(_path);
    // If the file exist, we can initiate font
    if(fontFile.exists()){
        m_oFontMessage.loadFont(fontFile.getAbsolutePath(), 30);

    }else{
        ofLogError() << "Font file does not exist : " << fontFile.getAbsolutePath();
    }
}

// Load Font For little Label sticked to the mouse ------------------------------------
void ruLibrary::loadFontLabel(string _path){
    // Loading font
    ofFile fontFile(_path);
    // If the file exist, we can initiate font
    if(fontFile.exists()){
        m_oFontLabel.loadFont(fontFile.getAbsolutePath(), 14, true, true, true);

    }else{
        ofLogError() << "Font file does not exist : " << fontFile.getAbsolutePath();
    }
}

// Load Font For little Label sticked to the 3D Mesh ------------------------------------
void ruLibrary::loadFontMesh(string _path){
    // Loading font
    ofFile fontFile(_path);
    // If the file exist, we can initiate font
    if(fontFile.exists()){
        m_oFontMesh.loadFont(fontFile.getAbsolutePath(), 8, true, true, true);
        // no change by defaut, but cool typologic options
        /*
         m_font.setLineHeight(34.0f);
         m_font.setLetterSpacing(1.035);
         */
    }else{
        ofLogError() << "Font file does not exist : " << fontFile.getAbsolutePath();
    }
}



// D- --------------------------------------------------------------------------------
void ruLibrary::getMeshMatrixPosition(int _idx, float _size, float _scale){

    float ratio = (float)_idx/m_aMeshes.size();

    ofRotate(360*ratio, 0, 1, 0);
    ofTranslate(0, 0, _size);
    ofScale(_scale, _scale, _scale);

}

void ruLibrary::drawAllBooks(float _size, float _scale){

    map<string, ruBook>::iterator          oneBookToDraw;
    vector<ofVec3f>::iterator               oneVertice;
    int idxVertice = 0;
    int idxBook = 0;

    m_aBooksByVertices.clear();

    //oneVertice = m_aOrderVerticesPosition.begin();
    oneVertice = (*m_oCurrentMesh).getVertices().begin();
    oneBookToDraw = m_aBooks_dates.begin();

    // Loop on books ------------------------------
    do {
        //ofPoint position = (*oneVertice).second;
        ofPoint position = (*oneVertice);

        // And draw it --
        float ratio = (float)idxBook/m_aBooks_dates.size();

        ofPushMatrix();
        getMeshMatrixPosition((*oneBookToDraw).second.get_idxMesh(), _size, _scale);
            (*oneBookToDraw).second.drawMesh(m_oFontMesh, position, m_oColorSet.getCurrentSetByProgress(ratio));
        ofPopMatrix();

        // We add the book with the vertice number in an array. This will give the book back when we get the vertice
        m_aBooksByVertices[idxVertice] = (*oneBookToDraw).second.get_isbnKey();

        oneVertice++;
        oneBookToDraw++;

        idxVertice++;
        idxBook++;

    } while (!( oneVertice==(*m_oCurrentMesh).getVertices().end() || oneBookToDraw==m_aBooks_dates.end() ));

}

void ruLibrary::drawLabel(ofEasyCam &_cam, ofPoint _pointerPos){

    // choose a point in the mesh
    int nbVertices = (*m_oCurrentMesh).getNumVertices();

    // DRAW THE LABEL FOR THE BOOK POINTED BY THE MOUSE
	float   nearestDistance = 0;
	ofPoint nearestVertex;
	int     nearestIndex = 0;
	//ofPoint mousePos(ofGetMouseX(), ofGetMouseY());

    for(int i = 0; i < nbVertices; i++) {
		ofVec3f cur = _cam.worldToScreen((*m_oCurrentMesh).getVertex(i));
		float distance = cur.distance(_pointerPos);
		if(i == 0 || distance < nearestDistance) {
			nearestDistance = distance;
			nearestVertex = cur;
			nearestIndex = i;
		}
	}

    // Style Start -------------------------------------------------------------------
    ofPushStyle();
    ofSetColor(m_oColorSet.getCurrentSetByIndex(0));

    ofNoFill();

    m_oPointerBegin.update(nearestVertex);
    m_oPointerBegin.draw();

    m_oPointerEnd.update(nearestVertex);
    m_oPointerEnd.draw();

    /*
    ofCircle(nearestVertex, 5);
    ofCircle(_pointerPos, 5);
*/

    ofLine(nearestVertex, _pointerPos);

    if(m_aBooksByVertices.find(nearestIndex) != m_aBooksByVertices.end()){
        // We get the isbn key back
        string isbnKey = m_aBooksByVertices[nearestIndex];
        // We draw the label of the book linked to the vertice
        m_aBooks[isbnKey].drawLabel(m_oFontLabel, _pointerPos + ofPoint(10,-10), m_oColorSet.getCurrentSetByIndex(1));
    }else{
        ofDrawBitmapString("Pas de livre, scannez le.", _pointerPos + ofPoint(10,-10));
        //ruBook::drawEmptyLabel(mousePos + ofPoint(10,-10), m_oColorSet.getCurrentSetByIndex(1));
    }


    ofPopStyle();
    // Style End -------------------------------------------------------------------

}

void ruLibrary::drawAllMeshWireframe_Front(int _alpha, float _size, float _scale){

    ofPushMatrix();

    for (unsigned int idxMesh=0; idxMesh!=m_aMeshes.size(); idxMesh++) {

        ofPushMatrix();
            getMeshMatrixPosition(idxMesh, _size, _scale);
            drawOneMeshWireframe_Front( _alpha, idxMesh);
        ofPopMatrix();

    }

    ofPopMatrix();

}

void ruLibrary::drawAllMeshPoints_Front(int _alpha, float _size, float _scale, float _pointSize){

    ofPushMatrix();

    for (unsigned int idxMesh=0; idxMesh!=m_aMeshes.size(); idxMesh++) {

        ofPushMatrix();
            getMeshMatrixPosition(idxMesh, _size, _scale);
            drawOneMeshPoints_Front( _alpha, _pointSize, idxMesh);
        ofPopMatrix();

    }

    ofPopMatrix();

}

void ruLibrary::drawOneMeshWireframe_Back(int _alpha){

    ofColor colorWrf = m_oColorSet.getCurrentSetByIndex(3);
    colorWrf.a = _alpha;

    // DRAW THE MESH
	ofPushStyle();

    ofSetColor(colorWrf);
    (*m_oCurrentMesh).drawWireframe();

    ofPopStyle();

}

void ruLibrary::drawOneMeshWireframe_Front(int _alpha, int _idxMesh){

    ofColor colorWrf = m_oColorSet.getCurrentSetByIndex(5);
    colorWrf.a = _alpha;

    // Choose the mesh
    vector<ofMesh>::iterator meshToDraw = m_oCurrentMesh;
    if (_idxMesh!=-1 && _idxMesh<(int)m_aMeshes.size()) {
        (*meshToDraw) = m_aMeshes[_idxMesh];
    }

    // DRAW THE MESH
	ofPushStyle();

    ofSetColor(colorWrf);
    (*meshToDraw).drawWireframe();

    ofPopStyle();

}

void ruLibrary::drawOneMeshPoints_Back(int _alpha, float _pointSize){

    ofColor colorPts = m_oColorSet.getCurrentSetByIndex(2);
    colorPts.a = _alpha;

    // DRAW THE MESH
	ofPushStyle();

    glPointSize(_pointSize);
    ofSetColor(colorPts);
    (*m_oCurrentMesh).drawVertices();

    ofPopStyle();

}
void ruLibrary::drawOneMeshPoints_Front(int _alpha, float _pointSize, int _idxMesh){

    ofColor colorPts = m_oColorSet.getCurrentSetByIndex(4);
    colorPts.a = _alpha;

    // DRAW THE MESH
	ofPushStyle();

    glPointSize(_pointSize);
    ofSetColor(colorPts);
    (*m_oCurrentMesh).drawVertices();

    ofPopStyle();

}



void ruLibrary::drawIsbnMessage(){
    /*
    string message = ofGetTimestampString("%Y %m %d %H:%M:%S  %i");
    message += " : ";
    message += getIsbnMessage();
     */

    string message = getIsbnMessage();

    ofPushStyle();
    ofPushMatrix();

    ofSetColor(m_oColorSet.getCurrentSetByIndex(0), 40);
    ofRectangle boundingBox = m_oFontMessage.getStringBoundingBox(message, 25, ofGetHeight() - 25);
    ofRect(boundingBox);

    ofPopMatrix();
    ofPopStyle();

    ofPushStyle();

    ofSetColor(m_oColorSet.getCurrentSetByIndex(6), 150);
    m_oFontMessage.drawString(message, 25, ofGetHeight() - 25 - boundingBox.getHeight());

    ofPopStyle();
}


string ruLibrary::toString(){

    string toString = "";

    map<string, ruBook>::iterator oneBookToDraw;

    // Loop on books ------------------------------
    for(oneBookToDraw=m_aBooks.begin(); oneBookToDraw!=m_aBooks.end(); oneBookToDraw++){
        // And draw it --
        toString += (*oneBookToDraw).second.toString() + "\n";
    }

    return toString;

}

// Loading/Saving as database
void ruLibrary::loadFromXml(string _path){

    ofFile fileToLoad(_path);
    int     nbBooks;

    if(!fileToLoad.exists()){
        ofLogError() << "This file : " << _path << "does not exists" << endl;
        return;
    }

    if(!m_oXml.loadFile(_path)){
        ofLogError() << "This file : " << _path << "can not be loaded" << endl;
        return;
    }

    m_aBooks.clear();

    if (m_oXml.pushTag("library")) {

        nbBooks = m_oXml.getNumTags("book");
        for(int idxBook=0; idxBook<nbBooks; idxBook++){

            ruBook bookToLoad;
            ofLogNotice() << "Book Loaded from XML : " << bookToLoad.toString() << endl;

            if(bookToLoad.loadFromXml(m_oXml, idxBook)){
                addABook(bookToLoad);
            }
        }
    }
}

void ruLibrary::saveToXml(){
    saveToXml(m_sFileName);
}

void ruLibrary::saveToXml(string _path){

    map<string, ruBook>::iterator oneBookToSave;
    int idxBook = 0;

    m_oXml.clear();
    m_oXml.addTag("library");

    /*
    if(!m_oXml.pushTag("library")){
        // Le Tag n'existe pas, on le crée
        m_oXml.addTag("library");
        m_oXml.pushTag("library");
    }
    */

    // Loop on books ------------------------------
    for(oneBookToSave=m_aBooks.begin(); oneBookToSave!=m_aBooks.end(); oneBookToSave++){
        // And save it --
        (*oneBookToSave).second.saveToXml(m_oXml, idxBook++);

        ofLogNotice() << "Book saved [" << idxBook << "]: " << (*oneBookToSave).second.toString() << endl;
    }

    if(!m_oXml.saveFile(_path)){
        ofLogError() << "This xml file : " << _path << " can not be saved" << endl;
        return;
    }

}

// --------------------------------------------
void ruLibrary::clear(bool _saveToXml){

    // clear vector of books
    m_aBooks.clear();

    // Eventually save it in the file
    if(_saveToXml==true)
        saveToXml(get_fileName());
}

// OVERRIDES From ofxIsbndb ----------------

// Receive and add a book
void ruLibrary::urlResponse(ofHttpResponse &_response){

    // The parent function decodes the book and fill the member bookReceived
    ofxIsbndb::urlResponse(_response);

    addLineIsbnMessage(getASuggestionSentence());
    addLineIsbnMessage(getARandomBook().stringMesh_Short());


    // add the book received
    ofxIsbndbBook roughBookReceived = get_BookReceived();

    if(roughBookReceived.get_isFilled()){
        ruBook        smartBookToAdd;

        smartBookToAdd.set_title(roughBookReceived.get_title());
        smartBookToAdd.set_author(roughBookReceived.get_author());
        smartBookToAdd.set_isbnKey(roughBookReceived.get_isbnKey());

        // Set the date
        smartBookToAdd.set_dateOfLastAdd(ofGetTimestampString("%Y%m%d%H%M%S%i"));

        // Add the book from API
        addABook(smartBookToAdd);
        // Save into XML
        saveToXml(get_fileName());
    }



}

// Sending too
/*
void ruLibrary::send(string _isbnNumber){
    // From ofxIsbndb
    ofxIsbndb::send(_isbnNumber);
}
*/

// To load a mesh
void ruLibrary::loadMesh(string _fileName){

    ofMesh _mesh;

    _mesh.load(_fileName);
    //m_oCurrentMesh = _mesh;

    if(_mesh.hasVertices()){
        m_aMeshes.push_back(_mesh);
        m_oCurrentMesh = m_aMeshes.begin();
    }

    nextMesh();

}

void ruLibrary::nextMesh(){

    m_oCurrentMesh++;
    m_iIdxMesh++;

    if(m_oCurrentMesh == m_aMeshes.end()){
        m_oCurrentMesh = m_aMeshes.begin();
        m_iIdxMesh     = 0;
    }

    m_aOrderVerticesPosition.clear();
    m_aOrderVerticesIndex.clear();

    for(int idxVertice=0; idxVertice<(*m_oCurrentMesh).getNumVertices(); idxVertice++){

        ofPoint vertice = (*m_oCurrentMesh).getVertex(idxVertice);
        string key = "";

        key += ofToString(abs(vertice.y), 0, 4, '0');
        key += "_";
        key += ofToString(abs(vertice.z), 0, 4, '0');
        key += "_";
        key += ofToString(abs(vertice.x), 0, 4, '0');

        //ofLogVerbose() << "Ordering Vertices [" << idxVertice << ":" << key;

        m_aOrderVerticesPosition[key] = vertice;
        m_aOrderVerticesIndex[key]    = idxVertice;
    }


}

// ---------------------------------------------------
// SUGGESTIONS DE BOUQUINS
void ruLibrary::addSuggestion(string _suggestion){
    m_aSuggestions.push_back(_suggestion);
}

string  ruLibrary::getASuggestionSentence(){

    int nextSuggestion = 0;

    if(m_aSuggestions.size()<=0)
        return "";

    do {
        nextSuggestion = (int)ofRandom(0, max((int)m_aSuggestions.size(), m_iLastSuggestionIdx));
        ofLogVerbose() << nextSuggestion << ":" << m_iLastSuggestionIdx;
    } while (nextSuggestion==m_iLastSuggestionIdx);

    m_iLastSuggestionIdx = nextSuggestion;

    return  m_aSuggestions[m_iLastSuggestionIdx];
}

// ---------------------------------------------------
ruBook  ruLibrary::getARandomBook(){

    int randomIdx = (int)ofRandom(0, m_aBooks.size());
    int idxReal = 0;

    map<string, ruBook>::iterator oneBook;

    for(oneBook = m_aBooks.begin(); oneBook != m_aBooks.end(); oneBook++){
        if(idxReal>=randomIdx) break;
        idxReal++;
    }

    return (*oneBook).second;

}
