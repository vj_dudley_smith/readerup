#include "testApp.h"

//--------------------------------------------------------------
void testApp::setup(){

    // STANDARD EAN13 : no prefix, no suffix, length : 13 caracters
    m_oCodbar = ofxCodbar("", "", 13);

    // Setup the library
    m_oLibrary.setup();
    
    // GUI ----------------------------------------
    setupGui();
    
    // Load the mesh
    m_oLibrary.loadMesh(guiPx_MeshFile);

    m_oLibrary.loadFromXml(m_oLibrary.get_fileName());
    // Route url event to library
    // We have to define the object for callback
    ofRegisterURLNotification(&m_oLibrary);
    //ofRegisterKeyEvents(&m_oCodbar);

    // ANIMATIONS AFTER SETTING LOADED FROM GUI ---
    setupAnimations();

    // GAME PADS -----------------------------------
    setupGamePads();

    // CAM ------------------------------------------
    // Move the cam, because first it is reverse
    camTiltSpeed = 0;
    camDollySpeed = 0;
    m_oCam.setTarget(ofPoint(300,0,0));
    /*
    m_oCam.setPosition(0,0,0);
    m_oCam.setTarget(ofPoint(0,0,300));
    m_oCam.setDistance(guiPx_CaravanSize + 200);
    */
    m_oPointerPos = ofPoint(0.5*ofGetWidth(), 0.5*ofGetHeight());

    // ARDUINO ---------------------------------------
    m_oArd.connect(guiPx_serialString, 57600);

    // listen for EInitialized notification. this indicates that
	// the arduino is ready to receive commands and it is safe to
	// call setupArduino()
	ofAddListener(m_oArd.EInitialized, this, &testApp::setupArduino);
	m_bSetupArduino	= false;	// flag so we setup arduino when its ready, you don't need to touch this :)

}

//--------------------------------------------------------------
void testApp::setupGui(){

    // --------------------------------------------------------------------------------------
    guiP_actions.setup("ACTIONS");
    guiP_actions.add(guiBT_saveImage.setup("Save png from view [s/S] "));
    guiP_actions.add(guiBT_loadColors.setup("Reload Colors"));
    guiP_actions.add(guiBT_addABookManually.setup("Add a book"));
    guiP_actions.add(guiBT_nextMesh.setup("Next Mesh"));

    guiP_actions.add(guiLb_WaitingTime.setup("Waiting time","Waiting Time"));


    // --------------------------------------------------------------------------------------
    guiP_settings.setup("Appearance", "appearanceSettings.xml", 250, 10);

    guiP_settings.add(  guiPx_Verbose.set("Verbose", false));
    guiP_settings.add(  guiPx_FullScreen.set("FullScreen", false));
    guiP_settings.add(  guiPx_mouseCursor.set("Show/Hide Cursor", true));
    guiP_settings.add(  guiPx_MeshFile.set("MeshFile", "Arbor.ply"));

    guiP_settings.add(  guiPx_BooksScale.set("Books Scale", 1.2, 0.5, 2.5));
    guiP_settings.add(  guiPx_BackMeshScale.set("Mesh Scale", 10, 2.5, 20));
    guiP_settings.add(  guiPx_BackMeshTranslate.set("Mesh Translate", 0, -100, 100));
    guiP_settings.add(  guiPx_MeshScaleRange.set("Mesh Scale Range", 2.5, 0, 5));
    guiP_settings.add(  guiPx_MeshScaleTimeSlow.set("Scale Time Slow", 10, 0, 20));
    guiP_settings.add(  guiPx_MeshScaleTimeFast.set("Scale Time Fast", 10, 0, 20));

    guiP_settings.add(  guiLb_Background.setup("Background", "Mesh Fond"));
    guiP_settings.add(  guiPx_BgAlphaPts.set("Bg Alpha points", 40, 0, 255));
    guiP_settings.add(  guiPx_BgAlphaWrf.set("Bg Alpha  wireframe", 10, 0, 255));
    guiP_settings.add(  guiPx_BgPointSize.set("Points Size", 2, 0, 10));

    guiP_settings.add(  guiLb_Main.setup("Main", "Main Mesh"));
    guiP_settings.add(  guiPx_MainAlphaPts.set("Main Alpha points", 40, 0, 255));
    guiP_settings.add(  guiPx_MainAlphaWrf.set("Main Alpha  wireframe", 10, 0, 255));
    guiP_settings.add(  guiPx_MainPointSize.set("Points Size", 2, 0, 10));
    guiP_settings.add(  guiPx_CaravanSize.set("Caravan Size", 800, 0, 1500));


    guiP_settings.loadFromFile("appearanceSettings.xml");

    // --------------------------------------------------------------------------------------
    cameraPanel.setup("Camera", "cameraSettings.xml", 10, 150);
    cameraPanel.add(guiLb_camDollySpeed.setup("Speed", "Speed"));
    cameraPanel.add(guiPx_camDollyMax.set("Max Dolly", 0, 0, 20));
    cameraPanel.add(guiPx_camDollyMin.set("Min Dolly", 0, 0, -20));
    cameraPanel.add(guiPx_camDollyStepForward.set("Step FW Dolly", 1, 0, 5));
    cameraPanel.add(guiPx_camDollyStepBackward.set("Step BW Dolly", 1, 0, 5));

    cameraPanel.add(guiLb_camPanSpeed.setup("Speed", "Speed"));
    cameraPanel.add(guiLb_camTiltSpeed.setup("Speed", "Speed"));
    cameraPanel.add(guiPx_camMax.set("Max Pan", 0, 0, 5));
    cameraPanel.add(guiPx_camMin.set("Min Pan", 0, 0, 5));
    cameraPanel.add(guiPx_camStepUp.set("Step Up", 0.05, 0, 0.1));
    cameraPanel.add(guiPx_camStepDn.set("Step Down", 0.05, 0, 0.1));
    cameraPanel.add(guiPx_pointerMoveFactor.set("Pointer factor", 1, -5, 5));

    cameraPanel.loadFromFile("cameraSettings.xml");


    // --------------------------------------------------------------------------------------
    guiP_player1.setup("Player_1", "player1Settings.xml", ofGetWidth() - 200, 10);

    guiP_player1.add(    guiLb_P1_Arrows.setup("Arrows","Fleches joueur 1"));
    guiP_player1.add(    guiPx_P1_UpDown.set("UP/DOWN", 0, -1, 1));
    guiP_player1.add(    guiPx_P1_LeftRight.set("LEFT/RIGHT", 0, -1, 1));

    guiP_player1.add(    guiPx_P1_AxisMode.set("Real Axis", false));

    guiP_player1.add(    guiPx_P1_NumUp.set("Up_numBt", 0, 0, 25));
    guiP_player1.add(    guiPx_P1_NumDn.set("Downn_numBt", 0, 0, 25));
    guiP_player1.add(    guiPx_P1_NumLt.set("Left_numBt", 0, 0, 25));
    guiP_player1.add(    guiPx_P1_NumRt.set("Right_numBt", 0, 0, 25));

    guiP_player1.add(    guiBt_P1_ResetMoves.setup("Reset"));

    guiP_player1.add(    guiLb_P1_Buttons.setup("Buttons","Boutons joueur 1"));
    guiP_player1.add(    guiLb_P1_LastBT.setup("Last P1",""));

    guiP_player1.add(    guiPx_P1_btnRed_NumOnPad.set("Red_numBt", 0, 0, 25));
    guiP_player1.add(    guiBT_P1_btnRed.set("Btn rouge [f1]", false));

    guiP_player1.add(    guiPx_P1_btnYe1_NumOnPad.set("Yellow1_numBt", 0, 0, 25));
    guiP_player1.add(    guiBT_P1_btnYellow_1.set("Btn jaune 1 [f2]", false));

    guiP_player1.add(    guiPx_P1_btnYe2_NumOnPad.set("Yellow2_numBt", 0, 0, 25));
    guiP_player1.add(    guiBT_P1_btnYellow_2.set("Btn jaune 2 [f3]", false));

    guiP_player1.add(    guiPx_P1_btnYe3_NumOnPad.set("Yellow3_numBt", 0, 0, 25));
    guiP_player1.add(    guiBT_P1_btnYellow_3.set("Btn jaune 3 [f4]", false));

    guiP_player1.loadFromFile("player1Settings.xml");


    // --------------------------------------------------------------------------------------
    arduinoCommands.setup("Arduino", "arduinoSettings.xml", 500, 10);

        arduinoCommands.add(    guiLb_animation.setup("Animation","Animation", false));
    arduinoCommands.add(    guiPx_serialString.set("Serial", "connect"));
    arduinoCommands.add(    guiPx_animHoDuration_normal.set("Speed Normal", 0, 0, 25));
    arduinoCommands.add(    guiPx_animHoDuration_scan.set("Speed Scan", 0, 0, 25));
    arduinoCommands.add(    guiPx_anim_Min.set("Min", 0, 0, 255));
    arduinoCommands.add(    guiPx_anim_Max.set("Max", 1, 0, 255));

        arduinoCommands.add(    guiLb_sends.setup("Sends view","Sends view", false));
    arduinoCommands.add(    guiPx_ardPin1.set("PIN 1", 0, 0, 13));
    arduinoCommands.add(    guiPx_ardSends1.set("Send 1", 0, 0, 255));
    arduinoCommands.add(    guiPx_ardPin2.set("PWM 2", 0, 0, 13));
    arduinoCommands.add(    guiPx_ardSends2.set("Send 2", 0, 0, 255));

    arduinoCommands.add(    guiBt_ardReset.setup("Reset Conn./Anims"));

    arduinoCommands.loadFromFile("arduinoSettings.xml");

    // -- Scheduling --
    Schedule.setup("Schedule", "Schedule", 500, 350);
    Schedule.add( guiPx_Monday.set("Monday", true));
    Schedule.add( guiPx_Tuesday.set("Tuesday", true));
    Schedule.add( guiPx_Wendesday.set("Wendesday", true));
    Schedule.add( guiPx_Thursday.set("Thursday", true));
    Schedule.add( guiPx_Friday.set("Friday", true));
    Schedule.add( guiPx_Saturday.set("Saturday", true));
    Schedule.add( guiPx_Sunday.set("Sunday", true));

    Schedule.add( guiPx_StartHour.set("Start", 9, 0, 23));
    Schedule.add( guiPx_EndHour.set("End", 19, 0, 23));

    Schedule.loadFromFile("scheduleSettings.xml");

}

//--------------------------------------------------------------
void testApp::setupAnimations(){
    // Reset the
    m_fLastUpdate = ofGetElapsedTimef();

    m_oAnim_Background.reset(guiPx_BackMeshScale);
    m_oAnim_Background.setCurve(EASE_IN_EASE_OUT);
    m_oAnim_Background.setRepeatType(LOOP_BACK_AND_FORTH);
    m_oAnim_Background.setDuration(guiPx_MeshScaleTimeSlow);
    m_oAnim_Background.animateFromTo(0, 1);
}

//--------------------------------------------------------------
void testApp::setupGamePads(){

    ofxGamepadHandler::get()->enableHotplug();

    //CHECK IF THERE EVEN IS A GAMEPAD CONNECTED
	if(ofxGamepadHandler::get()->getNumPads()>0){
        ofxGamepad* pad = ofxGamepadHandler::get()->getGamepad(0);
        ofAddListener(pad->onAxisChanged, this, &testApp::P1_axisChanged);
        // A bit weird, but event seems inverted ????
        #ifdef OF_TARGET_OSX
        ofAddListener(pad->onButtonPressed, this, &testApp::P1_buttonReleased);
        ofAddListener(pad->onButtonReleased, this, &testApp::P1_buttonPressed);
        #else
        ofAddListener(pad->onButtonPressed, this, &testApp::P1_buttonPressed);
        ofAddListener(pad->onButtonReleased, this, &testApp::P1_buttonReleased);
        #endif
	}
    

}

//--------------------------------------------------------------
void testApp::setupArduino(const int & version){

    m_oAnimLeds.setCurve(EASE_IN_EASE_OUT);
    m_oAnimLeds.setRepeatType(LOOP_BACK_AND_FORTH);
    m_oAnimLeds.setDuration(guiPx_animHoDuration_normal);
    m_oAnimLeds.animateFromTo(0, 1);

    // remove listener because we don't need it anymore
	ofRemoveListener(m_oArd.EInitialized, this, &testApp::setupArduino);

    // it is now safe to send commands to the Arduino
    m_bSetupArduino = true;

    // print firmware name and version to the console
    ofLogNotice() << m_oArd.getFirmwareName();
    ofLogNotice() << "firmata v" << m_oArd.getMajorFirmwareVersion() << "." << m_oArd.getMinorFirmwareVersion();
    ofLogNotice() << "Arduino OK. Setting Pins";

    // set pin 2-6 as PWM (analog output)
    m_oArd.sendDigitalPinMode(9, ARD_PWM);
    m_oArd.sendDigitalPinMode(10, ARD_PWM);

}

//--------------------------------------------------------------
void testApp::update(){

    // Update time elapsed since the last update
    float dtSec;

    dtSec = ofGetElapsedTimef() - m_fLastUpdate;
    m_fLastUpdate = ofGetElapsedTimef();

    // ANIMATIONS
    updateAnimations(dtSec);
    updateArduino(dtSec);

    // GUI --------------------------------------------------------------------
    updateGui();

    // When codbar is scanned, add the book and reopen the code
    if(m_oCodbar.isFullyScanned()){
        m_oLibrary.send(m_oCodbar.get_codbar());
        m_oCodbar.openCode();
    }

    // Now we move --------------------------------------------------------
    moveCamera(guiPx_P1_UpDown, -1*guiPx_P1_LeftRight, forwardPower, backwardPower);

    // Move camera : Inverted (Positive speed => Backward mouvement
    m_oCam.dolly(-camDollySpeed);
    m_oCam.pan(camPanSpeed);
    m_oCam.tilt(camTiltSpeed);
    // But the camera has to always look at center of the world
    //m_oCam.setTarget(ofPoint(0,0,0));
    m_oPointerPos = ofPoint(0.5*ofGetWidth(), 0.5*ofGetHeight());

}

void testApp::updateGui(){

    if(guiBT_saveImage)
        ofSaveFrame(true);

    if (guiBT_loadColors)
        m_oLibrary.m_oColorSet.loadFromXml("colorSets.xml");

    if (guiBT_addABookManually){
        // Ask the number on the back of a book and send it
        string isbnTyped = ofSystemTextBoxDialog("Type the number on the back of your book");
        m_oLibrary.send(isbnTyped);
    }

    if (guiBT_nextMesh==true) {
        m_oLibrary.nextMesh();
    }

    ofSetFullscreen(guiPx_FullScreen);

    if (guiPx_Verbose) {
        ofSetLogLevel(OF_LOG_VERBOSE);
    }else{
        ofSetLogLevel(OF_LOG_ERROR);
    }

    if(guiBt_P1_ResetMoves==true){
        guiPx_P1_UpDown = 0;
        guiPx_P1_LeftRight = 0;
    }


    //if(guiBt_ardReset)
        //setupArduino();

    if (guiBT_P1_btnRed==true) {

        // Change Colors
        m_oLibrary.m_oColorSet.nextSet();
    }
    if (guiBT_P1_btnYellow_1) {
        // Change Colors
        forwardPower=1;
    }else{
        forwardPower=0;
    }
    if (guiBT_P1_btnYellow_2) {
        // Change Meshes
        backwardPower=1;
    }else{
        backwardPower=0;
    }
    if (guiBT_P1_btnYellow_3) {
        m_oCam.reset();
    }

    // Display informations -----------------------------------------------
    guiLb_camDollySpeed = "Dolly : "    + ofToString(camDollySpeed);
    guiLb_camPanSpeed   = "Pan : "      + ofToString(camPanSpeed);
    guiLb_camTiltSpeed  = "Tilt : "     + ofToString(camTiltSpeed);

    guiLb_WaitingTime   = "Time : "     + ofToString(m_oLibrary.waitingTime(), 5, 2, '0');

    /*
    // Reset buttons Manually because Released event comes before Pressed event
    guiBT_P1_btnRed = false;
    guiBT_P1_btnYellow_1 = false;
    guiBT_P1_btnYellow_2 = false;
    guiBT_P1_btnYellow_3 = false;

    guiBT_P2_btnRed = false;
    guiBT_P2_btnYellow_1 = false;
    guiBT_P2_btnYellow_2 = false;
    guiBT_P2_btnYellow_3 = false;
    */

}

// -------------------------------------------------------------------------
void testApp::updateAnimations(float dt){

    // Animations ----------------------
    if(m_oLibrary.waitingTime()>0){
        m_oAnim_Background.setDuration(guiPx_MeshScaleTimeFast - m_oLibrary.waitingTime());
    }else{
        m_oAnim_Background.setDuration(guiPx_MeshScaleTimeSlow);
    }
    m_oAnim_Background.update(dt);

}

void testApp::moveCamera(float _upDownAxis, float _leftRightAxis, float _forward, float _backward){

    //ofLogVerbose() << "UP/DOWN" << _upDownAxis << "LEFT/RIGHT" << _leftRightAxis << "FW" << _forward << "BW" << _backward;

    // UP / DOWN -----------------------------------------------------------
    if(_upDownAxis != 0){
        // We push the joystick -----------------------------
        camTiltSpeed += _upDownAxis*guiPx_camStepUp;
        if(camTiltSpeed>guiPx_camMax)           camTiltSpeed=guiPx_camMax;
        if(camTiltSpeed<(-1.0*guiPx_camMin))    camTiltSpeed=(-1.0*guiPx_camMin);
    }else{
        // If we don't push, we have to stop
        if(abs(camTiltSpeed)< 2*guiPx_camStepDn){
            // The speed is enough to stop
            camTiltSpeed = 0;
        }else if(camTiltSpeed>0){
            // The speed is positive, we decrease -
            camTiltSpeed -= guiPx_camStepDn;
        }else if(camTiltSpeed<0){
            // The speed is negative, we increase -
            camTiltSpeed += guiPx_camStepDn;
        }
    }

    // LEFT / RIGHT -----------------------------------------------------------
    if(_leftRightAxis != 0){
        // We push the joystick -----------------------------
        camPanSpeed += _leftRightAxis*guiPx_camStepUp;

        if(camPanSpeed>guiPx_camMax)           camPanSpeed=guiPx_camMax;
        if(camPanSpeed<(-1.0*guiPx_camMin))    camPanSpeed=(-1.0*guiPx_camMin);

    }else{
        // If we don't push, we have to stop
        if(abs(camPanSpeed)< 2*guiPx_camStepDn){
            // The speed is enough to stop
            camPanSpeed = 0;
        }else if(camPanSpeed>0){
            // The speed is positive, we decrease -
            camPanSpeed -= guiPx_camStepDn;
        }else if(camPanSpeed<0){
            // The speed is negative, we increase -
            camPanSpeed += guiPx_camStepDn;
        }
    }

    // FORWARD / BACKWARD -----------------------------------------------------------
    if(_forward!=0 || _backward!=0){
        // We push the joystick -----------------------------
        camDollySpeed += _forward*guiPx_camDollyStepForward;
        camDollySpeed -= _backward*guiPx_camDollyStepBackward;

        if(camDollySpeed>guiPx_camDollyMax)             camDollySpeed=guiPx_camDollyMax;
        if(camDollySpeed<guiPx_camDollyMin)            camDollySpeed=guiPx_camDollyMin;

    }else{
        // If we don't push, we have to stop
        if(abs(camDollySpeed)< 2*guiPx_camDollyStepBackward){
            // The speed is enough to stop
            camDollySpeed = 0;
        }else if(camDollySpeed>0){
            // The camDollySpeed is positive, we decrease -
            camDollySpeed -= guiPx_camDollyStepBackward;
        }else if(camDollySpeed<0){
            // The speed is negative, we increase -
            camDollySpeed += guiPx_camDollyStepBackward;
        }
    }

}

//--------------------------------------------------------------
void testApp::updateArduino(float dt){

    float valueAnim;

    // Animations ----------------------
    if(m_oLibrary.waitingTime()>0){
        m_oAnimLeds.setDuration(guiPx_animHoDuration_scan);
    }else{
        m_oAnimLeds.setDuration(guiPx_animHoDuration_normal);
    }

    m_oAnimLeds.update(dt);
    valueAnim = ofMap(m_oAnimLeds.val(), 0, 1, guiPx_anim_Min, guiPx_anim_Max);

    guiPx_ardSends1 = valueAnim;
    guiPx_ardSends2 = valueAnim;

    if(isSchedulingOK()==false){
        valueAnim=0;
    }

    m_oArd.update();

    if(m_bSetupArduino){
        m_oArd.sendPwm(9, valueAnim);
        m_oArd.sendPwm(10, valueAnim);
    }

}

//--------------------------------------------------------------
void testApp::draw(){

    // First : Draw the background, because it is the background
    drawBackground();
    ofEnableSmoothing();

    if(isSchedulingOK()==true){

    // 3D Stuff --------------------------------------------------------
    m_oCam.begin();

    //ofCircle(0, 0, 50);

    // DRAW THE BOOKS
    ofPushMatrix();
    m_oLibrary.drawAllMeshWireframe_Front(guiPx_MainAlphaWrf, guiPx_CaravanSize, guiPx_BooksScale);
    m_oLibrary.drawAllMeshPoints_Front(guiPx_MainAlphaWrf, guiPx_CaravanSize, guiPx_BooksScale, guiPx_BgPointSize);
    m_oLibrary.drawAllBooks(guiPx_CaravanSize, guiPx_BooksScale);
    ofPopMatrix();


    // DRAW THE BackGround MESH
    ofPushMatrix();
    // Animation of the scale, and alpha
    float   realScale = ofMap(m_oAnim_Background.val(), 0, 1, guiPx_BackMeshScale-guiPx_MeshScaleRange, guiPx_BackMeshScale+guiPx_MeshScaleRange);
    // --
    ofScale(realScale, realScale, realScale);
    ofTranslate(0, guiPx_BackMeshTranslate);
    m_oLibrary.drawOneMeshWireframe_Back(guiPx_BgAlphaWrf);
    m_oLibrary.drawOneMeshPoints_Back(guiPx_BgAlphaPts, guiPx_MainPointSize);
    ofPopMatrix();

    m_oCam.end();


    // DRAW THE BOTTOM MESSAGE
    m_oLibrary.drawIsbnMessage();

    // DRAW THE LABEL Outside the cam
    m_oLibrary.drawLabel(m_oCam, m_oPointerPos);

    }

    // Last : Draw the gui if needed
    if(m_bDrawGui)  drawGui();

}

// All infos and stuff needed to debug and help the main user ------------------------------------------------
void testApp::drawGui(){

    int idxLineOfGui  = 0;
    int startPosOfGui = 100;
    int sizeLineOfGui = 20;

    guiP_actions.draw();
    guiP_settings.draw();
    cameraPanel.draw();
    arduinoCommands.draw();
    Schedule.draw();

    if(ofGetKeyPressed(' ')==false){
        guiP_player1.setTextColor(ofColor::paleGreen);
    }else{
        guiP_player1.setTextColor(ofColor::orangeRed);
    }

    guiP_player1.draw();

    // We announce what we do
    ofDrawBitmapString("READER UP !", 0.5*ofGetWidth(), 50);

    ofPushStyle();
        // Orange : Code incomplete
        // Green : Code OK
        if(m_oCodbar.isFullyScanned()){
            ofSetColor(ofColor::green);
        }else{
            ofSetColor(ofColor::orangeRed);
        }
        ofDrawBitmapString("Code Scanned : " + m_oCodbar.get_codbar(), 50, startPosOfGui+sizeLineOfGui*(idxLineOfGui++));
    ofPopStyle();

}

// This the background : Simple gradient ---------------------
void testApp::drawBackground(){
	ofBackgroundGradient(ofColor(64), ofColor(0));
}

//--------------------------------------------------------------
void testApp::keyPressed(int key){

    // Don't know why, i can't get the shift key event any more
    // ofLogVerbose() << "KEY PRESSED : " << key;

    if(ofGetKeyPressed(' ')==false){

        // PLAYER 1 : AQCV + F1...F4
        if(key==OF_KEY_UP)    guiPx_P1_UpDown=1;
        if(key==OF_KEY_LEFT)    guiPx_P1_LeftRight=1;
        if(key==OF_KEY_RIGHT)    guiPx_P1_LeftRight=-1;
        if(key==OF_KEY_DOWN)    guiPx_P1_UpDown=-1;

        if(key=='c')    guiBT_P1_btnRed=true;
        if(key=='v')    guiBT_P1_btnYellow_1=true;
        if(key=='b')    guiBT_P1_btnYellow_2=true;
        if(key=='n')    guiBT_P1_btnYellow_3=true;

    }

}

//--------------------------------------------------------------
void testApp::keyReleased(int key){


    if(ofGetKeyPressed(' ')==false){

        // PLAYER 1 : AQCV + F1...F4
        if(key==OF_KEY_UP)    guiPx_P1_UpDown=0;
        if(key==OF_KEY_LEFT)    guiPx_P1_LeftRight=0;
        if(key==OF_KEY_RIGHT)    guiPx_P1_LeftRight=0;
        if(key==OF_KEY_DOWN)    guiPx_P1_UpDown=0;

        if(key=='c')    guiBT_P1_btnRed=false;
        if(key=='v')    guiBT_P1_btnYellow_1=false;
        if(key=='b')    guiBT_P1_btnYellow_2=false;
        if(key=='n')    guiBT_P1_btnYellow_3=false;

    }

    // Add the key released to codbar
    m_oCodbar.addKeyToCode(key);
    // Message from isbn
    //m_oLibrary.setIsbnMessage(m_oCodbar.get_codbar());

    // --------------------------------------------------
    if(key == '$'){
        m_bDrawGui = !m_bDrawGui;
        if (m_bDrawGui) {
            ofShowCursor();
        }else{
            ofHideCursor();
        }
    }
    // --------------------------------------------------
    if(key == 's' || key=='S')
        ofSaveFrame(true);
            // --------------------------------------------------
    if(key == 'f' || key=='F'){
        if(guiPx_FullScreen==true){
            guiPx_FullScreen=false;
        }else{
            guiPx_FullScreen=true;
        }
    }

}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void testApp::P1_axisChanged(ofxGamepadAxisEvent& e){

    ofLogVerbose() << "AXIS " << e.axis << " VALUE " << ofToString(e.value);

    if (guiPx_P1_AxisMode==true) {
        if(e.axis==1)   guiPx_P1_UpDown=e.value;
        if(e.axis==0)   guiPx_P1_LeftRight= e.value;
    }


}

void testApp::P1_buttonPressed(ofxGamepadButtonEvent& e){
	ofLogVerbose() << "BUTTON " << e.button << " PRESSED";
    guiLb_P1_LastBT = ofToString(e.button);

    if (guiPx_P1_AxisMode==false) {
        if(e.button == guiPx_P1_NumUp)           guiPx_P1_UpDown=1;
        if(e.button == guiPx_P1_NumDn)           guiPx_P1_UpDown=-1;
        if(e.button == guiPx_P1_NumLt)           guiPx_P1_LeftRight=1;
        if(e.button == guiPx_P1_NumRt)           guiPx_P1_LeftRight=-1;
    }

    if(e.button == guiPx_P1_btnRed_NumOnPad) guiBT_P1_btnRed=true;
    if(e.button == guiPx_P1_btnYe1_NumOnPad) guiBT_P1_btnYellow_1 = true;
    if(e.button == guiPx_P1_btnYe2_NumOnPad) guiBT_P1_btnYellow_2 = true;
    if(e.button == guiPx_P1_btnYe3_NumOnPad) guiBT_P1_btnYellow_3 = true;
}

void testApp::P1_buttonReleased(ofxGamepadButtonEvent& e){
    ofLogVerbose() << "BUTTON " << e.button << " RELEASED";
    guiLb_P1_LastBT = ofToString(e.button);

    if (guiPx_P1_AxisMode==false) {
        if(e.button == guiPx_P1_NumUp)           guiPx_P1_UpDown=0;
        if(e.button == guiPx_P1_NumDn)           guiPx_P1_UpDown=0;
        if(e.button == guiPx_P1_NumLt)           guiPx_P1_LeftRight=0;
        if(e.button == guiPx_P1_NumRt)           guiPx_P1_LeftRight=0;
    }

    if(e.button == guiPx_P1_btnRed_NumOnPad) guiBT_P1_btnRed=false;
    if(e.button == guiPx_P1_btnYe1_NumOnPad) guiBT_P1_btnYellow_1 = false;
    if(e.button == guiPx_P1_btnYe2_NumOnPad) guiBT_P1_btnYellow_2 = false;
    if(e.button == guiPx_P1_btnYe3_NumOnPad) guiBT_P1_btnYellow_3 = false;
}


bool testApp::isSchedulingOK(){

    bool dayOK = false;
    bool timeOK = false;

    int weekDay = ofGetWeekday();

    switch(weekDay){
        case 1:
            if (guiPx_Monday==true)   dayOK=true;
            break;
        case 2:
            if (guiPx_Tuesday==true)   dayOK=true;
            break;
        case 3:
            if (guiPx_Wendesday==true)   dayOK=true;
            break;
        case 4:
            if (guiPx_Thursday==true)  dayOK=true;
            break;
        case 5:
            if (guiPx_Friday==true)   dayOK=true;
            break;
        case 6:
            if (guiPx_Saturday==true)   dayOK=true;
            break;
        case 7:
            if (guiPx_Sunday==true)   dayOK=true;
            break;
        default:
            dayOK=true;
            break;
    }

    if(ofGetHours()>=guiPx_StartHour && ofGetHours()<guiPx_EndHour){
        timeOK = true;
    }

    return timeOK && dayOK;


}
