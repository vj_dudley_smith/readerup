#pragma once

#define USE_OIS

#include "ofMain.h"
#include "ofxGui.h"

#include "ofxAnimatableFloat.h"

#include "ofxGamepad.h"
#include "ofxGamepadHandler.h"

#include "ofxCodbar.h"
#include "ruLibrary.h"
#include "ofxMousePointers.h"

class testApp : public ofBaseApp{

public:
    void setup();
    void update();
    void draw();

    void keyPressed  (int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y );

    // ---------------------------------------------------
    // MEMBERS -------------------------------------------
//GUI----------------------------------------------
    bool m_bDrawGui;

    ofxPanel    guiP_actions;
    ofxButton   guiBT_saveImage;
    ofxButton   guiBT_loadColors;
    ofxButton   guiBT_addABookManually;
    ofxButton   guiBT_nextMesh;

    ofxLabel    guiLb_WaitingTime;


    ofxPanel    guiP_settings;

    ofParameter<bool>   guiPx_FullScreen;
    ofParameter<bool>   guiPx_Verbose;
    ofParameter<bool>   guiPx_mouseCursor;
    ofParameter<string>     guiPx_MeshFile;
    ofParameter<float>      guiPx_BooksScale;
    ofParameter<float>      guiPx_BackMeshScale;
    ofParameter<float>      guiPx_BackMeshTranslate;
    ofParameter<float>      guiPx_MeshScaleRange;
    ofParameter<float>      guiPx_MeshScaleTimeSlow;
    ofParameter<float>      guiPx_MeshScaleTimeFast;

    ofxLabel                guiLb_Background;
    ofParameter<int>        guiPx_BgAlphaPts;
    ofParameter<int>        guiPx_BgAlphaWrf;
    ofParameter<float>      guiPx_BgPointSize;

    ofxLabel                guiLb_Main;
    ofParameter<int>        guiPx_MainAlphaPts;
    ofParameter<int>        guiPx_MainAlphaWrf;
    ofParameter<float>      guiPx_MainPointSize;
    ofParameter<float>      guiPx_CaravanSize;


    ofxPanel cameraPanel;
    ofxLabel                guiLb_camDollySpeed;
    ofParameter<float>      guiPx_camDollyMax;
    ofParameter<float>      guiPx_camDollyMin;
    ofParameter<float>      guiPx_camDollyStepForward;
    ofParameter<float>      guiPx_camDollyStepBackward;

    ofxLabel                guiLb_camPanSpeed;
    ofxLabel                guiLb_camTiltSpeed;
    ofParameter<float>      guiPx_camMax;
    ofParameter<float>      guiPx_camMin;
    ofParameter<float>      guiPx_camStepUp;
    ofParameter<float>      guiPx_camStepDn;
    ofParameter<float>      guiPx_pointerMoveFactor;

    ofxPanel guiP_player1;
        ofxLabel                guiLb_P1_Arrows;
    ofParameter<float>       guiPx_P1_UpDown;
    ofParameter<float>       guiPx_P1_LeftRight;
    ofParameter<bool>        guiPx_P1_AxisMode;
    ofxButton                guiBt_P1_ResetMoves;

    ofParameter<int>        guiPx_P1_NumUp;
    ofParameter<int>        guiPx_P1_NumDn;
    ofParameter<int>        guiPx_P1_NumLt;
    ofParameter<int>        guiPx_P1_NumRt;

        ofxLabel                guiLb_P1_Buttons;
        ofxLabel                guiLb_P1_LastBT;
    ofParameter<int>        guiPx_P1_btnRed_NumOnPad;
    ofParameter<bool>       guiBT_P1_btnRed;
    ofParameter<int>        guiPx_P1_btnYe1_NumOnPad;
    ofParameter<bool>       guiBT_P1_btnYellow_1;
    ofParameter<int>        guiPx_P1_btnYe2_NumOnPad;
    ofParameter<bool>       guiBT_P1_btnYellow_2;
    ofParameter<int>        guiPx_P1_btnYe3_NumOnPad;
    ofParameter<bool>       guiBT_P1_btnYellow_3;

    ofxPanel                Schedule;
    ofParameter<bool>       guiPx_Monday;
    ofParameter<bool>       guiPx_Tuesday;
    ofParameter<bool>       guiPx_Wendesday;
    ofParameter<bool>       guiPx_Thursday;
    ofParameter<bool>       guiPx_Friday;
    ofParameter<bool>       guiPx_Saturday;
    ofParameter<bool>       guiPx_Sunday;

    ofParameter<int>        guiPx_StartHour;
    ofParameter<int>        guiPx_EndHour;

    // Represents Arduino Sends ---------------------------
    ofxPanel arduinoCommands;

    ofxLabel                guiLb_animation;
    ofParameter<string>     guiPx_serialString;

    ofParameter<float>      guiPx_animHoDuration_normal;

    ofParameter<float>      guiPx_animHoDuration_scan;

    ofParameter<float>      guiPx_anim_Min;
    ofParameter<float>      guiPx_anim_Max;

    ofxLabel                guiLb_sends;
    ofParameter<float>        guiPx_ardSends1;
    ofParameter<float>        guiPx_ardSends2;

    ofParameter<int>          guiPx_ardPin1;
    ofParameter<int>          guiPx_ardPin2;

    ofxButton                   guiBt_ardReset;

// From Addons ---------------------------------------------------
    ofxCodbar   m_oCodbar;
    ruLibrary   m_oLibrary;

    // CAMERA --------------------------------------------
    ofEasyCam   m_oCam;


    float   forwardPower, backwardPower;
    float   camDollySpeed, camPanSpeed, camTiltSpeed;

    void    moveCamera(float _upDownAxis, float _leftRightAxis, float _forward, float _backward);
    // ---------------------------------------------------

    // ---------------------------------------------------
    // ANIMATION
    void setupAnimations();
    void updateAnimations(float dt);

    float m_fLastUpdate;
    ofxAnimatableFloat m_oAnim_Background;
    // ---------------------------------------------------

    // ---------------------------------------------------
    // METHODS
    void drawGui();
    void updateGui();
    void setupGui();

    void drawBackground();

    void addABookWithQuestion();
    void fillRandom();
    // ---------------------------------------------------

    // ---------------------------------------------------
    // GAME PAD
    void setupGamePads();

    ofxGamepad m_oP1;

    void P1_axisChanged(ofxGamepadAxisEvent &e);
    void P1_buttonPressed(ofxGamepadButtonEvent &e);
    void P1_buttonReleased(ofxGamepadButtonEvent &e);


    // ---------------------------------------------------

    // ---------------------------------------------------
    // POINTER
    ofPoint m_oPointerPos;

    // ---------------------------------------------------
    // LED Animations
    void setupArduino(const int & version);
    //void setupArduino();
    void updateArduino(float dt);

    ofxAnimatableFloat m_oAnimLeds;
    ofArduino          m_oArd;
    bool               m_bSetupArduino;


    // SCHEDULING ---
    bool isSchedulingOK();
};
